"use strict";

module.exports = {
  "groupName": "Misc",
  "properties": [
    "cursor",
    "opacity",
    "z-index",
    "overflow",
    "overflow-x",
    "overflow-y",
    "visibility",
    "pointer-events",
    "appearance",
    "clip",
    "clip-path",
    "counter-reset",
    "counter-increment",
    "resize",
    "user-select",
    "will-change",
    "zoom",
    "fill",
    "fill-rule",
    "clip-rule",
    "stroke",
    "filter",
    "mix-blend-mode"
  ]
}
