"use strict";

module.exports = {
  "groupName": "Height",
  "properties": [
    "height",
    "min-height",
    "max-height"
  ]
}
