"use strict";

module.exports = {
  "groupName": "Table Layout",
  "properties": [
    "table-layout",
    "empty-cells",
    "caption-side"
  ]
}
