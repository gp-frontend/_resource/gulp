"use strict";

module.exports = {
  "groupName": "Width",
  "properties": [
    "width",
    "min-width",
    "max-width"
  ]
}
