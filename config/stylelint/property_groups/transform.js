"use strict";

module.exports = {
  "groupName": "Transform",
  "properties": [
    "transform",
    "transform-origin",
    "transform-style",
    "backface-visibility",
    "perspective",
    "perspective-origin"
  ]
}
