"use strict";

module.exports = {
  "groupName": "Box alignment child rules",
  "order": "flexible",
  "properties": [
    "flex",
    "flex-grow",
    "flex-shrink",
    "flex-basis",
    "align-self",
    "justify-self",
    "order"
  ]
}
