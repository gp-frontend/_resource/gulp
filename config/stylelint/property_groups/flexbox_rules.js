"use strict";

module.exports = {
  "groupName": "Flexbox",
  "order": "flexible",
  "properties": [
    "flex-flow",
    "flex-direction",
    "flex-wrap"
  ]
}
