"use strict";

module.exports = {
  "groupName": "Shadows",
  "properties": [
    "box-shadow",
    "box-decoration-break",
    "text-shadow"
  ]
}
