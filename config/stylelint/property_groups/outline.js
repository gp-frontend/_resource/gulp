"use strict";

module.exports = {
  "groupName": "Outline",
  "properties": [
    "outline",
    "outline-width",
    "outline-style",
    "outline-color",
    "outline-offset"
  ]
}
